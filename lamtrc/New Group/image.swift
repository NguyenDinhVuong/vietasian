//
//  image.swift
//  lamtrc
//
//  Created by Nguyễn Đình Vương on 12/7/18.
//  Copyright © 2018 Nguyễn Đình Vương. All rights reserved.
//

import Foundation
import ObjectMapper

class image: Mappable{
    required init?(map: Map) {
        
    }
    
    var url:String=""
    var discription:String=""
    
    func mapping(map: Map){
        url                     <- map["url"]
        discription             <- map["description"]
    }
}
