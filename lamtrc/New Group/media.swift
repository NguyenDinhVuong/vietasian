//
//  media.swift
//  lamtrc
//
//  Created by Nguyễn Đình Vương on 12/7/18.
//  Copyright © 2018 Nguyễn Đình Vương. All rights reserved.
//

import Foundation
import ObjectMapper

class media: Mappable{
    required init?(map: Map) {
        
    }
    
    var image : image!
    var video : video!
    var status : Int = 0
    
    func mapping(map: Map){
        image               <- map["image"]
        video               <- map["video"]
        status              <- map["status"]
    }
}
