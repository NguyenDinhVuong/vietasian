//
//  user.swift
//  lamtrc
//
//  Created by Nguyễn Đình Vương on 12/7/18.
//  Copyright © 2018 Nguyễn Đình Vương. All rights reserved.
//

import Foundation
import ObjectMapper

class user: Mappable{
    required init?(map: Map) {
        
    }
    
    var accesstoken:String=""
    var userID:String=""
    var userName:String=""
    var fullName:String=""
    
    func mapping(map: Map){
        accesstoken                 <- map["access_token"]
        userID                      <- map["user_id"]
        userName                    <- map["user_name"]
        fullName                    <- map["full_name"]
    }
}
