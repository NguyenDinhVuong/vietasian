//
//  UIViewExtensions.swift
//  Passio Coffee
//
//  Created by Hoang Vu on 6/27/18.
//  Copyright © 2018 Passio. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    func setRadius(radius: CGFloat? = nil) {
        self.layer.cornerRadius = radius ?? self.frame.width / 2;
        self.layer.masksToBounds = true;
    }
}

