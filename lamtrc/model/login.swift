//
//  login.swift
//  lamtrc
//
//  Created by Nguyễn Đình Vương on 12/7/18.
//  Copyright © 2018 Nguyễn Đình Vương. All rights reserved.
//

import Foundation

class login {
    var userName:String=""
    var fullName:String=""
    var userID:Int=0
    var accesstoken:String=""
    
    func mapping(){
        userName                    <- map["user_name"]
        userID                      <- map["user_id"]
        fullName                    <- map["full_name"]
        accesstoken                 <- map["access_token"]
    }
}
