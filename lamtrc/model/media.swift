//
//  data.swift
//  lamtrc
//
//  Created by Nguyễn Đình Vương on 12/7/18.
//  Copyright © 2018 Nguyễn Đình Vương. All rights reserved.
//

import Foundation

class media {
    var image : image!
    var video : video!
    var status : Int = 0
    
    func mapping(){
        image               <- map["image"]
        video               <- map["video"]
        status              <- map["status"]
    }
}
