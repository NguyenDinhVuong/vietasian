//
//  Constant.swift
//  Passio Coffee
//
//  Created by Hoang Vu on 6/26/18.
//  Copyright © 2018 Passio. All rights reserved.
//

import Foundation


class AppURL {
    static let Host                     = "http://210.2.93.10:30782/api/viet-asian/"
    
    static let login                    = Host + "user/login"
    static let register                 = Host + "user/register"
    static let media                    = Host + "media"
    static let video                    = Host + "video"
}

let BrandId = "1"

class AppStoryboard {
    static let Main = "Main"
}

class AppIdentifier {
    static let InfoViewController                       = "InfoViewController"
    static let RegisterViewController                   = "RegisterViewController"
}

class Currency {
    static let VND = "đ"
    static let point = "điểm"
    static let passioPoint = "P"
    static let percent = "%"
}

class ColorPallet {
    static let sickGreen = ConvertManager.hexStringToUIColor(hex: "#a6ce39")
    static let lightMustard = ConvertManager.hexStringToUIColor(hex: "#f7c65e")
    static let pastelRed = ConvertManager.hexStringToUIColor(hex: "#db5d52")
    static let charcoal = ConvertManager.hexStringToUIColor(hex: "#363e2e")
    static let silver = ConvertManager.hexStringToUIColor(hex: "#b5b7b7")
    static let darkSage = ConvertManager.hexStringToUIColor(hex: "#53584e")
    static let darkSkyBlue = ConvertManager.hexStringToUIColor(hex: "#4a90e2")
    static let denimBlue = ConvertManager.hexStringToUIColor(hex: "#3b579d")
}

class RankName {
    static let rankName = ["Green": "mới", "Silver": "bạc", "Gold": "vàng"]
}

