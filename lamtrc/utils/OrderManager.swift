//
//  OrderManager.swift
//  Passio Coffee
//
//  Created by UniMob on 7/26/18.
//  Copyright © 2018 Passio. All rights reserved.
//

import Foundation
import RealmSwift

class OrderManager {
    
    static func setDefaultRealmForUser(username: String) {
        var config = Realm.Configuration()
        
        // Use the default directory, but replace the filename with the username
        config.fileURL = config.fileURL!.deletingLastPathComponent().appendingPathComponent("\(username).realm")
        
        // Set this as the configuration used for the default Realm
        Realm.Configuration.defaultConfiguration = config
    }
    
//    static func getOrder(order:OrderItem) -> OrderItem? {
//        let realm = try! Realm()
//        return realm.objects(OrderItem.self).first
//    }
    
//    static func getAllOrder() -> [OrderItem]? {
//        let realm = try! Realm()
//        return Array(realm.objects(OrderItem.self).detached)
//        let realm = try! Realm()
//        var orderItemList:[OrderItem] = []
//        for item in realm.objects(OrderItem.self) {
//            orderItemList.append(OrderItem(value: item))
//        }
//        return orderItemList
//    }

//    static func createInfo(order:OrderItem) {
//        let realm = try! Realm()
//        try! realm.write {
//            realm.add(order)
//        }
//    }
//
//    static func deleteOrder(order:OrderItem) {
//        let realm = try! Realm()
//        let str = "orderItemId == " + "\"" + order.orderItemId + "\""
//        let objDelete = realm.objects(OrderItem.self).filter(str).first
//        try! realm.write {
//            realm.delete(objDelete!)
//        }
//    }
//
//    static func deleteAllOrder() {
//        let realm = try! Realm()
//        try! realm.write {
//            realm.delete(realm.objects(OrderItem.self))
//        }
//    }
//
//    static func updateOrder(order:OrderItem) {
//        let realm = try! Realm()
//        try! realm.write {
//            realm.add(order, update: true)
//        }
//    }
//
//    static func addOrder(order:OrderItem) {
//        let realm = try! Realm()
//        let str = "orderItemId == " + "\"" + order.orderItemId + "\""
//        let obj = realm.objects(OrderItem.self).filter(str).first
//        let price = (obj?.subTotal)!/Double((obj?.quantity)!)
//        try! realm.write {
//            obj?.quantity = (obj?.quantity)! + 1
//            obj?.subTotal = Double((obj?.quantity)!) * price
//            realm.add(obj!, update: true)
//        }
//    }
//
//    static func subOrder(order:OrderItem) {
//        let realm = try! Realm()
//        let str = "orderItemId == " + "\"" + order.orderItemId + "\""
//        let obj = realm.objects(OrderItem.self).filter(str).first
//        let price = (obj?.subTotal)!/Double((obj?.quantity)!)
//        try! realm.write {
//            obj?.quantity = (obj?.quantity)! - 1
//            obj?.subTotal = Double((obj?.quantity)!) * price
//            realm.add(obj!, update: true)
//        }
//    }
//
//    static func choiceColor(order:OrderItem) {
//        let realm = try! Realm()
//        let str = "orderItemId == " + "\"" + order.orderItemId + "\""
//        let obj = realm.objects(OrderItem.self).filter(str).first
//        try! realm.write {
//            obj?.productExtra=order.productExtra
//            realm.add(obj!, update: true)
//        }
//    }
}

