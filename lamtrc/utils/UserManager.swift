//
//  PersionalInformationManager.swift
//  Passio Coffee
//
//  Created by UniMob on 7/20/18.
//  Copyright © 2018 Passio. All rights reserved.
//

import Foundation
import RealmSwift

class UserManager {

    static func setDefaultRealmForUser(username: String) {
        var config = Realm.Configuration()
        
        // Use the default directory, but replace the filename with the username
        config.fileURL = config.fileURL!.deletingLastPathComponent().appendingPathComponent("\(username).realm")
        
        // Set this as the configuration used for the default Realm
        Realm.Configuration.defaultConfiguration = config
    }
    
    //user
//    static func getInfo() -> PersonalInformation? {
//        let realm = try! Realm()
//        return realm.objects(PersonalInformation.self).first
//    }
//
//    static func createInfo(info:PersonalInformation) {
//        let realm = try! Realm()
//        try! realm.write {
//            realm.add(info)
//        }
//    }
//
//    static func deleteAllInfo() {
//        let realm = try! Realm()
//        try! realm.write {
//            realm.deleteAll()
//        }
//    }

//    static func updateInfo(name:String, phone:String, email:String, birthday:String, gender:Bool, avatar:String) {
//        let realm = try! Realm()
//        let info = self.getInfo()
//        try! realm.write {
//            info?.name = name
//            info?.phone = phone
//            info?.email = email
//            info?.firstLogin = false
//            info?.birthDay = birthday
//            info?.gender = gender
//            info?.picUrl = avatar
//        }
//    }
    
//    static func updateInfo(data:PersonalInformation, accessToken:String) {
//        let realm = try! Realm()
//        let info = self.getInfo()
//        try! realm.write {
//            info?.name = data.name
//            info?.phone = data.phone
//            info?.email = data.email
//            info?.birthDay = data.birthDay
//            info?.gender = data.gender
//            info?.picUrl = data.picUrl
//            info?.accessToken=accessToken
//            info?.membershipCard=data.membershipCard
//        }
//    }
//
//    static func updateAvatar(picUrl: String) {
//        let realm = try! Realm()
//        let info = self.getInfo()
//        if info != nil {
//            try! realm.write {
//                info?.picUrl = picUrl
//            }
//        }
//    }
//
//    static func updateMembershipCard(code:String, typeName:String, typeLevel:Int, balance:Int, point:Int) {
//        let realm = try! Realm()
//        let info = self.getInfo()
//        try! realm.write {
//            info?.code = code
//            info?.typeName = typeName
//            info?.typeLevel = typeLevel
//            info?.balance = balance
//            info?.point = point
//        }
//    }
//
//    static func updateAccessToken(accessToken:String) {
//        let realm = try! Realm()
//        let info = self.getInfo()
//        try! realm.write {
//            info?.accessToken = accessToken
//        }
//    }
}
