//
//  InfoViewController.swift
//  lamtrc
//
//  Created by Nguyễn Đình Vương on 12/7/18.
//  Copyright © 2018 Nguyễn Đình Vương. All rights reserved.
//

import UIKit

class InfoViewController: UIViewController {
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var fullname: UILabel!
    @IBOutlet weak var userid: UILabel!
    @IBOutlet weak var accesstoken: UILabel!
    
    var user:user! = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        username.text=user.userName
        fullname.text=user.fullName
        userid.text=user.userID
        accesstoken.text=user.accesstoken
    }
    
    func loadMedia() {
        let media:MediaBehavior=MediaAPIHelper()
        media.getMedia(date: "06-12-2018", code: 100).on(
            failed: {   error in
                print("faild")
        },
            value: { media in
                print(media)
        }
            ).start()
    }
    
    func loadVideo() {
        let media:MediaBehavior=MediaAPIHelper()
        media.getVideo(skip: 0, take: 10).on(
            failed: {   error in
                print("faild")
        },
            value: { media in
                print(media)
        }
            ).start()
    }
    
    @IBAction func media(_ sender: Any) {
        loadMedia()
    }
    @IBAction func random(_ sender: Any) {
        loadVideo()
    }
}
