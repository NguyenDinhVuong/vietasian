//
//  RegisterViewController.swift
//  lamtrc
//
//  Created by Nguyễn Đình Vương on 12/8/18.
//  Copyright © 2018 Nguyễn Đình Vương. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {
    @IBOutlet weak var txtconfirm: UITextField!
    @IBOutlet weak var txtpassword: UITextField!
    @IBOutlet weak var txtusername: UITextField!
    @IBOutlet weak var txtfullName: UITextField!
    var user:user!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    func register(fullname:String, username:String, password:String, confirm:String) {
        let login:UserBehavior=UserAPIHelper()
        login.register(email: username, password: password, confirmPassword: confirm, fullName: fullname).on(
            failed: {   error in
                print("faild")
        },
            value: { user in
                self.user=user
                let register = AppHelper.loadViewControllerFromStoryboard(storyboardName: AppStoryboard.Main, withIdentifier: AppIdentifier.InfoViewController) as! InfoViewController
                register.user=self.user
                self.navigationController?.pushViewController(register, animated: true)
        }
            ).start()
    }
    
    @IBAction func btnregister(_ sender: Any) {
        self.register(fullname: txtfullName.text!, username: txtusername.text!, password: txtpassword.text!, confirm: txtconfirm.text!)
    }
    
    
}
