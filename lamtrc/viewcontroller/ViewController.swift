//
//  ViewController.swift
//  lamtrc
//
//  Created by Nguyễn Đình Vương on 12/7/18.
//  Copyright © 2018 Nguyễn Đình Vương. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    
    var user:user!
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    func loadLogin(username:String,password:String) {
        let login:UserBehavior=UserAPIHelper()
        login.login(username: username, password: password).on(
            failed: {   error in
                print("faild")
        },
            value: { user in
            self.user=user
            let login = AppHelper.loadViewControllerFromStoryboard(storyboardName: AppStoryboard.Main, withIdentifier: AppIdentifier.InfoViewController) as! InfoViewController
                login.user=self.user
                self.navigationController?.pushViewController(login, animated: true)
        }
            ).start()
    }
    @IBAction func btnLogin(_ sender: Any) {
        loadLogin(username: username.text!, password: password.text!)
    }
    @IBAction func btnregister(_ sender: Any) {
        let register = AppHelper.loadViewControllerFromStoryboard(storyboardName: AppStoryboard.Main, withIdentifier: AppIdentifier.RegisterViewController) as! RegisterViewController
        
        self.navigationController?.pushViewController(register, animated: true)
    }
}

