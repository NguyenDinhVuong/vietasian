//
//  MediaAPIHelper.swift
//  lamtrc
//
//  Created by Nguyễn Đình Vương on 12/7/18.
//  Copyright © 2018 Nguyễn Đình Vương. All rights reserved.
//

import Foundation
import ReactiveSwift
import Alamofire
import ObjectMapper

class MediaAPIHelper: BaseAPIHelper, MediaBehavior {
    func getMedia(date:String, code:Int) -> SignalProducer<media, NSError> {
        return SignalProducer<media, NSError>{
            (sink,_) in
            let requestURL = AppURL.media+"/get?date=\(date)&code=\(code)"
            self.alamofireManager.request(requestURL, method: .get, encoding: JSONEncoding.default, headers: self.header).responseJSON(completionHandler: { response in
                
                if response.response?.statusCode == 200, let value = response.result.value as? [String : Any]{
                    if let success = value["success"] as? Bool, success == true {
                        if let responseData = value["data"] as? [String: Any]{
                            let media = Mapper<media>().map(JSON: responseData)
                            sink.send(value: media!)
                            sink.sendCompleted()
                        }else{
                            let error = NSError(domain: "Data Error", code: 0, userInfo: nil)
                            sink.send(error: error)
                        }
                    }else{
                        let error = NSError(domain: "Data Error", code: 0, userInfo: nil)
                        sink.send(error: error)
                    }
                }
                else{
                    let error = NSError(domain: "Server Error", code: 0, userInfo: nil)
                    sink.send(error: error)
                }
            })
        }
    }
    
    func getVideo(skip:Int, take:Int) -> SignalProducer<video, NSError> {
        return SignalProducer<video, NSError>{
            (sink,_) in
            let requestURL = AppURL.video+"/random?skip=\(skip)&take=\(take)"
            self.alamofireManager.request(requestURL, method: .get, encoding: JSONEncoding.default, headers: self.header).responseJSON(completionHandler: { response in
                
                if response.response?.statusCode == 200, let value = response.result.value as? [String : Any]{
                    if let success = value["success"] as? Bool, success == true {
                        if let responseData = value["data"] as? [String: Any]{
                            let video = Mapper<video>().map(JSON: responseData)
                            sink.send(value: video!)
                            sink.sendCompleted()
                        }else{
                            let error = NSError(domain: "Data Error", code: 0, userInfo: nil)
                            sink.send(error: error)
                        }
                    }else{
                        let error = NSError(domain: "Data Error", code: 0, userInfo: nil)
                        sink.send(error: error)
                    }
                }
                else{
                    let error = NSError(domain: "Server Error", code: 0, userInfo: nil)
                    sink.send(error: error)
                }
            })
        }
    }
}
