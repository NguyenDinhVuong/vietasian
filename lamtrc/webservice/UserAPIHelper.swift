//
//  UserAPIHelper.swift
//  lamtrc
//
//  Created by Nguyễn Đình Vương on 12/7/18.
//  Copyright © 2018 Nguyễn Đình Vương. All rights reserved.
//

import Foundation
import ReactiveSwift
import Alamofire
import ObjectMapper

class UserAPIHelper: BaseAPIHelper, UserBehavior {
    func register(email: String, password: String, confirmPassword: String, fullName: String) -> SignalProducer<user, NSError> {
        return SignalProducer<user, NSError>{
            (sink,_) in
            let parameters = ["username":email,"password":password,"confirm_password":confirmPassword,"fullName":fullName,"brand_id":BrandId] as Dictionary<String, Any>
            let requestURL = AppURL.register
            self.alamofireManager.request(requestURL, method: .post, parameters:parameters, encoding: JSONEncoding.default, headers: self.header).responseJSON(completionHandler: { response in
                
                if response.response?.statusCode == 200, let value = response.result.value as? [String : Any]{
                    if let success = value["status"] as? Bool, success == true {
                        if let responseData = value["data"] as? [String: Any]{
                            let user = Mapper<user>().map(JSON: responseData)
                            sink.send(value: user!)
                            sink.sendCompleted()
                        }else{
                            let error = NSError(domain: "Data Error", code: 0, userInfo: nil)
                            sink.send(error: error)
                        }
                    }else{
                        let error = NSError(domain: "Data Error", code: 0, userInfo: nil)
                        sink.send(error: error)
                    }
                }
                else{
                    let error = NSError(domain: "Server Error", code: 0, userInfo: nil)
                    sink.send(error: error)
                }
            })
        }
    }
    
    func login(username: String, password: String) -> SignalProducer<user, NSError> {
        return SignalProducer<user, NSError>{
            (sink,_) in
            let parameters = ["username":username, "password":password] as Dictionary<String, Any>
            let requestURL = AppURL.login
            self.alamofireManager.request(requestURL, method: .post, parameters:parameters, encoding: JSONEncoding.default, headers: self.header).responseJSON(completionHandler: { response in
                
                if response.response?.statusCode == 200, let value = response.result.value as? [String : Any]{
                    if let success = value["success"] as? Bool, success == true {
                        if let responseData = value["data"] as? [String: Any]{
                            let user = Mapper<user>().map(JSON: responseData)
                            sink.send(value: user!)
                            sink.sendCompleted()
                        }else{
                            let error = NSError(domain: "Data Error", code: 0, userInfo: nil)
                            sink.send(error: error)
                        }
                    }else{
                        let error = NSError(domain: "Data Error", code: 0, userInfo: nil)
                        sink.send(error: error)
                    }
                }
                else{
                    let error = NSError(domain: "Server Error", code: 0, userInfo: nil)
                    sink.send(error: error)
                }
            })
        }
    }
}
