//
//  File.swift
//  Uni Deli
//
//  Created by UniMob on 9/27/18.
//  Copyright © 2018 Reso. All rights reserved.
//

import Foundation
import ReactiveSwift

protocol UserBehavior {
    func login(username:String, password:String) -> SignalProducer<user,NSError>
    func register(email:String, password:String, confirmPassword:String, fullName:String) -> SignalProducer<user,NSError>
}

protocol MediaBehavior {
    func getMedia(date:String, code:Int) -> SignalProducer<media,NSError>
    func getVideo(skip:Int, take:Int) -> SignalProducer<video,NSError>
}
